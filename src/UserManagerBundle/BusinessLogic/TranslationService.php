<?php
/**
 * Created by PhpStorm.
 * User: naeem
 * Date: 24.05.19
 * Time: 11:37
 */

namespace UserManagerBundle\BusinessLogic;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use UserManagerBundle\Entity\translations;

class TranslationService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * TranslationService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param array $params
     * @return array
     */
    public function addImprints(Request $request)
    {
        if(empty($request->get('locale'))){

            return array(
                'data' => "locale must be provided",
                'status' => 403
            );
        }
        if(empty($request->get('text'))){

            return array(
                'data' => "text must be provided",
                'status' => 403
            );
        }
        $os = array("en", "de", "nl");
        if (!in_array($request->get('locale'), $os)) {

            return array(
                'data' => "locale must be en, de,nl",
                'status' => 403
            );
        }

        try{
            $trans = new translations();
            $trans->setText($request->get('text'));
            $trans->setLocale($request->get('locale'));
            $this->em->persist($trans);
            $this->em->flush($trans);

            return array(
                'data' => "imprint added",
                'status' => 201
            );
        } catch (\Doctrine\ORM\ORMException $ex){

            return array(
                'data' => $ex->getMessage(),
                'status' => 500
            );
        }

    }

    /**
     * @param Request $request
     * @return array
     */
    public function getImprints(Request $request)
    {
        return $this->em->createQueryBuilder()
            ->addSelect("imprints.locale, imprints.text")
            ->from('UserManagerBundle:translations', 'imprints')
            ->where('imprints.locale = :locale')
            ->setParameter('locale', $request->get('_locale'))
            ->getQuery()
            ->getArrayResult();
    }

}