<?php

namespace UserManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use UserManagerBundle\BusinessLogic\TranslationService;
use UserManagerBundle\BusinessLogic\UserService;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class UserController extends Controller
{

    /**
     * @Route("/users")
     */
    public function listUsersAction(UserService $userService)
    {
        $result = $userService->listUsers();

        return $this->render('@UserManager/listUsers.html.twig', ['users' =>$result]);
    }

    /**
     * @Route("/add-text/")
     */
    public function imprintAction(UserService $userService)
    {
        return $this->render('@UserManager/addImprints.html.twig');

    }

    /**
     * @Route("/add-imprint")
     * @Method({"POST"})
     */
    public function addImprintAction(Request $request, TranslationService $transService)
    {
        $result = $transService->addImprints($request);
        $response = new JsonResponse();
        $response->setStatusCode($result['status']);
        $response->setData($result['data']);

        return $response;
    }

    /**
     * @Route("/{_locale}/imprints",
     *     requirements={
     *         "_locale": "en|de|nl"
     *     })
     * @Method({"POST"})
     */
    public function listImprintAction(Request $request, TranslationService $transService)
    {
        $result = $transService->getImprints($request);

        return $this->render('@UserManager/imprints.html.twig', ['imprints' =>$result]);

    }
}
